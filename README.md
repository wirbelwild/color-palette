[![Codacy Badge](https://api.codacy.com/project/badge/Grade/4e739429871046ef861a8b34f2c28c4d)](https://www.codacy.com?utm_source=bitbucket.org&amp;utm_medium=referral&amp;utm_content=wirbelwild/color-palette&amp;utm_campaign=Badge_Grade)
[![License](https://poser.pugx.org/bitandblack/colorpalette/license)](https://packagist.org/packages/bitandblack/colorpalette)
[![PHP version](https://badge.fury.io/ph/bitandblack%2Fcolorpalette.svg)](https://badge.fury.io/ph/bitandblack%2Fcolorpalette)
[![npm version](https://badge.fury.io/js/bitandblack-colorpalette.svg)](https://badge.fury.io/js/bitandblack-colorpalette)

# Color Palette

Hex values of all PANTONE, HKS and RAL colors.

## Installation

This package is available for [Composer](https://packagist.org/packages/bitandblack/colorpalette) and also for [Node](https://www.npmjs.com/package/bitandblack-colorpalette).

### Yarn 

Add it to your project by running `$ yarn add bitandblack-colorpalette`.

### NPM

Add it to your project by running `$ npm install bitandblack-colorpalette`.

### Composer

Add it to your project by running `$ composer require bitandblack/colorpalette`.

## Usage

### SCSS / SASS 

The preferred way to add all those colors to your project is by including one or more of the `scss` files from the `src` folder. 

````scss
@import "~bitandblack-colorpalette/src/pantone/pantone-plus-solid-coated";

body {
  color: map_get($pantone-plus-solid-coated, "pantone-7547-c");
}
````

This will result in:

````css
body {
    color: #121f29;
}
````

If you don't have the possibility to use the `scss` files it's also possible to use the `css` files from the `dist` folder.

### JavaScript 

The color values can also be used with JavaScript:

````javascript
import variables from "bitandblack-colorpalette/src/pantone/pantone-plus-solid-coated.scss";

console.log(variables["pantone-7547-c"]);
````

This will dump `#121f29`.

## Help 

If you have any questions feel free to contact us under `hello@bitandblack.com`.